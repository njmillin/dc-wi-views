<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php

  /**
   * This view is overriding the nothing field for https://online.uwc.edu/admin/proctor-email.
   * It finds and replaces the campus proctor names.
   *
   */



  // originally what it was.
  //print $output;    
  
  
  // Trimming the whitespace b/c that was messing up the switch statement.
  $output = trim ($output);

  
  // These emails need to be updated from https://online.uwc.edu/node/610/webform/emails/2
  // Need to have the campus emails instead of the campus names. This is more future proof than changing the keys every time there is a new email address.
  switch ($output) {
    case "UW-Baraboo / Sauk County":
        print "UW-Baraboo / Sauk County<br>matthew.warming@uwc.edu";
        break;
    case "UW-Barron County":
        print "UW-Barron County<br>gary.phillips@uwc.edu";
        break;
    case "UW-Fond du Lac":
        print "UW-Fond du Lac<br>FDL-Proctor@uwc.edu";
        break;
    case "UW-Fox Valley":
        print "UW-Fox Valley<br>FOX-Proctor@uwc.edu";
        break;
    case "UW-Manitowoc":
        print "UW-Manitowoc<br>man-proctoring@uwc.edu";
        break;
    case "UW-Marathon County":
        print "UW-Marathon County<br>MTHProctor@uwc.edu";
        break;
    case "UW-Marinette":
        print "UW-Marinette<br>Jack.Mlsna@uwc.edu";
        break;
    case "UW-Marshfield / Wood County":
        print "UW-Marshfield / Wood County<br>robert.apfel@uwc.edu";
        break;
    case "UW-Richland":
        print "UW-Richland<br>marilyn.peckham@uwc.edu";
        break;
    case "UW-Rock County":
        print "UW-Rock County<br>rck-proctoring@uwc.edu";
        break;
    case "UW-Sheboygan":
        print "UW-Sheboygan<br>SHBTestingCenter@uwc.edu";
        break;
    case "UW-Washington County":
        print "UW-Washington County<br>WSH-Proctoring@uwc.edu";
        break;
    case "UW-Waukesha":
        print "UW-Waukesha<br>david.weber@uwc.edu";
        break;
    
    // default for when it wasn't a campus proctor
    default:
        print $output;
  }
?>