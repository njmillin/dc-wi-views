/**
 * Implementation of Drupal behavior.
 */
(function($) {
Drupal.behaviors.rubik = {};
Drupal.behaviors.rubik.attach = function(context) {
  /**
   * Event listener for when someone changes an Event Date it will update
   * the end date to be the same.
   */
  
  // Set date to use bellow
  $updated_date = $("#edit-field-event-dates-und-0-value-datepicker-popup-0").val();

  // Add the 'dummy' date field that isn't editable
  $("#edit-field-event-dates-und-0-value2-datepicker-popup-0_uwc").remove(); // makes sure that it is only adding one field instead of multiples when you exclude/include dates.
  $("#edit-field-event-dates-und-0-value2 .form-item-field-event-dates-und-0-value2-date").append("<input type='text' id='edit-field-event-dates-und-0-value2-datepicker-popup-0_uwc' value='" + $updated_date + "' class='form-text disabled-field' disabled>"); 
  
  // Change the description to disabled.
  $("#edit-field-event-dates-und-0-value2 .description").html("The start & end date will always be the same.");
  
  // update date when it changes
  $("#edit-field-event-dates-und-0-value-datepicker-popup-0").change(function () {
    //alert("you just changed it! " + $(this).val() + "---");
    $updated_date = $(this).val();
    $("#edit-field-event-dates-und-0-value2-datepicker-popup-0").val($updated_date);
    $("#edit-field-event-dates-und-0-value2-datepicker-popup-0_uwc").val($updated_date);
  });
  
  /* Junk stuff that I tried and doesn't work. */
  //$("#edit-field-event-dates-und-0-value2-datepicker-popup-0").prop("disabled", true);
  
  // add the readonly attribute to the input field.
  //$("#edit-field-event-dates-und-0-value2-datepicker-popup-0").attr("readonly", "readonly");
  
  // add the readonly attribute to the input field.
  //$("#edit-field-event-dates-und-0-value2-datepicker-popup-0").addClass("disabled-field");
  
};
})(jQuery);
